import os
import logging.config
import tornado
import requests
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.web import Application, RequestHandler


BASE_DIR = os.path.dirname(os.path.dirname(__file__))
LOGS_DIR = os.path.join(BASE_DIR, 'logs')
if not os.path.exists(LOGS_DIR):
    os.mkdir(LOGS_DIR)
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        },
        'access': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': LOGS_DIR + "/access.log",
            'maxBytes': 50000,
            'backupCount': 3,
            'formatter': 'standard',
        },
        'app': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': LOGS_DIR + "/app.log",
            'maxBytes': 50000,
            'backupCount': 3,
            'formatter': 'standard',
        },
        'general': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': LOGS_DIR + "/general.log",
            'maxBytes': 50000,
            'backupCount': 3,
            'formatter': 'standard',
        },
        'debug': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': LOGS_DIR + "/debug.log",
            'maxBytes': 50000,
            'backupCount': 3,
            'formatter': 'standard',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {
        "handlers": {
            "level": "DEBUG",
            "propagate": False,
            "handlers": ["debug", "console"]
        },
        "tornado.access": {
            "level": "INFO",
            "propagate": False,
            "handlers": ["access", "console"]
        },
        "tornado.application": {
            "level": "INFO",
            "propagate": False,
            "handlers": ["app", "console"]
        },
        "tornado.general": {
            "level": "INFO",
            "propagate": False,
            "handlers": ["general", "console"]
        },
    }
}
logging.config.dictConfig(LOGGING)


class ForwardHandler(RequestHandler):
    def get(self):
        print('eeee!')
        self.write({})

    def post(self):
        response = requests.post('http://localhost:8855/sendmsg', self.request.body)
        print(response.content)


urls = ((r'/sendmsg', ForwardHandler),)


def main():
    # rt = '/home/jut/tgbot'
    rt = '/root'
    # host = '127.0.0.1'
    host = '178.62.249.118'

    logging.info('Starting http server...')
    app = Application(urls, debug=True)
    server = tornado.httpserver.HTTPServer(app, xheaders=True, ssl_options={
        'certfile': '%s/tgbot.pub.pem' % rt,
        'keyfile': '%s/tgbot.key' % rt,
    })
    server.bind(port=88, address=host)

    server.start(1)
    logging.info('Server started')
    tornado.ioloop.IOLoop.current().start()


if __name__ == '__main__':
    main()
