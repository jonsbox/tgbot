import os
import json
import mimetypes
from uuid import uuid4
from functools import partial
from tornado.httpclient import AsyncHTTPClient, HTTPRequest
from tornado.httputil import url_concat
from tornado import gen


with open(os.path.join(os.getenv('HOME'), 'tgbot', 'token')) as f:
    TOKEN = f.read()
    TG_API = f'https://api.telegram.org/bot{TOKEN}/'


class Sender(object):
    async def run_command(self, command, method, data):
        url = TG_API + command
        method = method.upper()
        if method == 'GET':
            if data:
                url = url_concat(url, data)
                data = None
        elif method == 'POST' and isinstance(data, dict):
            data = json.dumps(data)
        request = {
            'url': url,
            'method': method,
            'headers': {"content-type": "application/json"},
            'body': data
        }
        client = AsyncHTTPClient()
        try:
            response = await client.fetch(HTTPRequest(**request))
            return json.load(response.buffer)
        except BaseException as e:
            return {
                "ok": False,
                "error_code": None,
                "description": str(e)
            }

    async def send_msg(self, reciever, text):
        pars = {
            'chat_id': reciever,
            'text': text
        }
        return await self.run_command('sendMessage', 'POST', pars)

    async def file_info(self, file_id):
        pars = {
            'file_id': file_id
        }
        return await self.run_command('getFile', 'POST', pars)

    async def download_file(self, tg_path, destination):
        url = f'https://api.telegram.org/file/bot{TOKEN}/{tg_path}'
        client = AsyncHTTPClient()
        try:
            response = await client.fetch(HTTPRequest(url))
            with open(destination, 'wb') as f:
                f.write(response.buffer.read())
            return {'ok': True}
        except BaseException as e:
            return {
                "ok": False,
                "error_code": None,
                "description": str(e)
            }

    @gen.coroutine
    def _files_uploader(self, boundary, filenames, params, write):
        boundary_bytes = boundary.encode()

        for (filename, filepath) in filenames.items():
            filename_bytes = filename.encode()
            write(b'--%s\r\n' % (boundary_bytes,))
            write(b'Content-Disposition: form-data; name="%s"; filename="%s"\r\n' %
                  (filename_bytes, filename_bytes))

            mtype = mimetypes.guess_type(filename)[0] or 'application/octet-stream'
            write(b'Content-Type: %s\r\n' % (mtype.encode(),))
            write(b'\r\n')
            with open(filepath, 'rb') as f:
                while True:
                    # 16k at a time.
                    chunk = f.read(16 * 1024)
                    if not chunk:
                        break
                    write(chunk)

                    # Let the IOLoop process its event queue.
                    yield gen.moment

            write(b'\r\n')
            yield gen.moment

        for argname in params:
            value = params[argname]
            write(b'--%s\r\n' % (boundary_bytes,))
            write(b'Content-Disposition: form-data; name="%s"\r\n\r\n%s\r\n' %
                  (argname.encode(), value.encode()))

        write(b'--%s--\r\n' % (boundary_bytes,))

        write(b'--%s--\r\n' % (boundary_bytes,))

    async def send_photo(self):
        boundary = uuid4().hex
        headers = {'Content-Type': 'multipart/form-data; boundary=%s' % boundary}
        url = TG_API + 'sendPhoto'
        client = AsyncHTTPClient()
        filenames = {
            'photo': '/home/jut/Изображения/diversity-154704_640.png'
        }
        params = {
            'chat_id': '1298111265',
            'caption': 'Каптион'
        }
        producer = partial(self._files_uploader, boundary, filenames, params)
        response = await client.fetch(url,
                                      method='POST',
                                      headers=headers,
                                      body_producer=producer)

        print(response)


# import asyncio
# sender = Sender()
# loop = asyncio.get_event_loop()
# # res = loop.run_until_complete(sender.run_command('getUpdates', 'get', {'a': 1, 'b': 2}))
# res = loop.run_until_complete(sender.send_msg(1298111265, 'test script'))
# print(res)
# loop.close()

