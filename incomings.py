import os
import json
from datetime import datetime
from tornado.web import RequestHandler
from sender import Sender


class IncomingHandler(RequestHandler):
    def _retrieve_commands(self):
        if not self.message or 'text' not in self.message:
            self.commands = []
            return

        text = self.message['text']
        self.commands = [f'{text[e["offset"] + 1: e["offset"] + e["length"]]}' for e in self.message.get('entities', [])
                         if e['type'] == 'bot_command']

    async def prepare(self):
        data = json.loads(self.request.body)
        # print(data)
        self.data = data
        self.message = data['message'] if 'message' in data else data['edited_message']
        self._retrieve_commands()
        self.document = self.message.get('document', None)
        self.photo = self.message.get('photo', None)
        self.tg = None

    @property
    def chat(self):
        return self.message['chat']

    @property
    def sender(self):
        return self.message['from']

    @property
    def api(self):
        if not self.tg:
            self.tg = Sender()
        return self.tg

    async def _get_file_info(self, file_id):
        return await self.api.file_info(file_id)

    async def _download_photo(self):
        photo_inf = self.photo
        # TODO: предусмотреть ошибки
        if not photo_inf:
            return
        caption = self.message.get('caption', str(datetime.now()))
        finfo = await self._get_file_info(photo_inf[0]['file_id'])
        if not finfo['ok']:
            return
        path = finfo['result']['file_path']
        ext = os.path.splitext(path)[1]
        dest = os.path.join('/tmp', caption + ext)
        await self.api.download_file(path, dest)
        return dest

    async def post(self):
        if self.photo:
            await self.api.send_msg(self.chat['id'], 'Чо ты мне фотки шлёшь?')
        else:
            await self.api.send_msg(self.chat['id'], 'Без фоток грустно')
            await self.api.send_photo()
        # path = await self._download_photo()
        # print(path)
